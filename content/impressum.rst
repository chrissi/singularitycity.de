sort: 10000
menu: Impressum & Datenschutz
template: page.html


Impressum
=========


Verantwortlich für den Inhalt dieser Seite nach §6Abs.2 MDStV ist die SFMW UG
(haftungsbeschränkt) vertreten durch ihren Geschäftsführer René Stegmaier.
Die SMFW UG (haftungsbeschränkt) ist beim Amtsgericht Hildesheim unter der
Registernummer 204219 ins Handelsregister eingetragen.

| SMFW UG (haftungsbeschränkt)
| Ummersche Heerstr. 5
| 38518 Gifhorn 


Datenschutzerklärung
====================

Benennung der verantwortlichen Stelle
-------------------------------------

Die verantwortliche Stelle für die Datenverarbeitung auf dieser Website ist:

| SMFW UG
| René Stegmaier
| Ummersche Heerstraße 5
| 38518 Gifhorn
|

Die verantwortliche Stelle entscheidet allein oder gemeinsam mit anderen über
die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten
(z.B. Namen, Kontaktdaten o. Ä.).

Widerruf Ihrer Einwilligung zur Datenverarbeitung
-------------------------------------------------

Nur mit Ihrer ausdrücklichen Einwilligung sind einige Vorgänge der
Datenverarbeitung möglich.
Ein Widerruf Ihrer bereits erteilten Einwilligung ist jederzeit möglich.
Für den Widerruf genügt eine formlose Mitteilung per E-Mail.
Die Rechtmäßigkeit der bis zum Widerruf erfolgten Datenverarbeitung bleibt vom
Widerruf unberührt.

Recht auf Beschwerde bei der zuständigen Aufsichtsbehörde
---------------------------------------------------------

Als Betroffener steht Ihnen im Falle eines datenschutzrechtlichen Verstoßes ein
Beschwerderecht bei der zuständigen Aufsichtsbehörde zu.
Zuständige Aufsichtsbehörde bezüglich datenschutzrechtlicher Fragen ist der
Landesdatenschutzbeauftragte des Bundeslandes, in dem sich der Sitz unseres
Unternehmens befindet.
Der folgende Link stellt eine Liste der Datenschutzbeauftragten sowie deren
Kontaktdaten bereit:
`https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html <https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html>`__.

Recht auf Datenübertragbarkeit
------------------------------

Ihnen steht das Recht zu, Daten, die wir auf Grundlage Ihrer Einwilligung oder
in Erfüllung eines Vertrags automatisiert verarbeiten, an sich oder an Dritte
aushändigen zu lassen.
Die Bereitstellung erfolgt in einem maschinenlesbaren Format.
Sofern Sie die direkte Übertragung der Daten an einen anderen Verantwortlichen
verlangen, erfolgt dies nur, soweit es technisch machbar ist.

Recht auf Auskunft, Berichtigung, Sperrung, Löschung
----------------------------------------------------

Sie haben jederzeit im Rahmen der geltenden gesetzlichen Bestimmungen das Recht
auf unentgeltliche Auskunft über Ihre gespeicherten personenbezogenen Daten,
Herkunft der Daten, deren Empfänger und den Zweck der Datenverarbeitung und
ggf. ein Recht auf Berichtigung, Sperrung oder Löschung dieser Daten.
Diesbezüglich und auch zu weiteren Fragen zum Thema personenbezogene Daten
können Sie sich jederzeit über die im Impressum aufgeführten
Kontaktmöglichkeiten an uns wenden.

Server-Log-Dateien
------------------

In Server-Log-Dateien erhebt und speichert der Provider der Website automatisch
Informationen, die Ihr Browser automatisch an uns übermittelt.
Dies sind:

* Besuchte Seite auf unserer Domain
* Datum und Uhrzeit der Serveranfrage
* Browsertyp und Browserversion
* Verwendetes Betriebssystem
* Referrer URL
* Hostname des zugreifenden Rechners
* IP-Adresse

Es findet keine Zusammenführung dieser Daten mit anderen Datenquellen statt.
Grundlage der Datenverarbeitung bildet Art. 6 Abs. 1 lit. b DSGVO, der die
Verarbeitung von Daten zur Erfüllung eines Vertrags oder vorvertraglicher
Maßnahmen gestattet.

