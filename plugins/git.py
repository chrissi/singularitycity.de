import subprocess
import os

class Git:

    def contents_parsed(self, context):
        state = subprocess.getoutput(["git log -1 --pretty=format:'%H %aI'"])
        for content in context.contents:
            content["git_state"] = state

            path = os.path.join("content", content["path"])
            updated = subprocess.getoutput(["git log -1 --pretty=format:'%aI' {}".format(path)])
            content["git_update"] = updated

            try:
                with open("REF_SLUG") as fh:
                    branch = fh.readlines()[0].strip()
            except:
                branch = subprocess.getoutput(["git branch | grep \* | cut -d ' ' -f2"])

            if "HEAD" in branch:
                branch = "master"
            content["git_branch"] = branch
