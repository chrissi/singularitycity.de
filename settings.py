import os

PLUGINS = [
    'flamingo.plugins.Redirects',
    'flamingo.plugins.rstBootstrap3',
    'flamingo.plugins.rstPygments',
    'plugins/title.py::Title',
    'plugins/git.py::Git',
]

POST_BUILD_LAYERS = [
        'static',
]

THEME_PATHS = [
        'theme/',
]

